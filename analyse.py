#!/usr/bin/env python

from lxml import etree
import sys
import os.path
import argparse
import logging
import ConfigParser
import requests
from datetime import date
from datetime import datetime

class counter:
    """A counter for outputting progress in --info mode"""
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000

    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))

class Analyser:
    def __init__(self, nexamples, namespace=None, entity_type=None, entity_attrib=None):
        if namespace is not None and entity_type is not None:
            try:
                self.namespace = { namespace.split(':::')[0]: namespace.split(':::')[1] }
            except IndexError:
                log.error('Is your namespace in the correct form: {prefix}:::{namespace declaration}')
                log.error('Note the ":::" delimiting the prefix and namespace')
                sys.exit()
            self.entity_type = entity_type
            self.entity_attribute = entity_attrib
        else:
            self.namespace = None
            self.entity_type = None

        self.etypes = {}
        self.nexamples = int(nexamples)

    def process(self, path):
        """Iterate over a directory of harvested datafiles
        
        @params:
        path: the directory of content to be analysed
        """
        for (dirpath, dirnames, filenames) in os.walk(path):
            total = len(filenames)
            
            i = 0
            c = counter(total)
            for fname in filenames:
                self.process_document("%s/%s" % (dirpath, fname))
                i += 1
                c.update(i)
        
        print "Found the following entity types in this feed"
        for key, value in self.etypes.items():
            print "%s: %s records" % (key, len(value))
            for i in range(self.nexamples):
                try:
                    print "\t%s" % (value[i])
                except IndexError:
                    continue
                

    def process_document(self, document):
        """Process a single datafile

        @params:
        document: the file to be processed
        """
        if self.namespace is not None:
            try:
                doc = etree.parse(document)
                if self.entity_attribute is not None:
                    entity_type = doc.xpath(self.entity_type, namespaces=self.namespace)[0].attrib[self.entity_attribute]
                else:
                    entity_type = doc.xpath(self.entity_type, namespaces=self.namespace)[0].text
                #print doc.xpath(self.entity_type, namespaces=self.namespace)[0].attrib
            except IndexError:
                #print sys.exc_info()
                log.error("Couldn't determine entity type of %s" % document)
                return
            except etree.XMLSyntaxError:
                log.error("Screwey document; check it! %s" % document)
                return

        else:
            try:
                entity_type = document.split('-')[2]
            except IndexError:
                log.error("I can't seem to get the entity type from %s, skipping it for now" % document)
                return None

        try:
            document_list = self.etypes[entity_type]
        except KeyError:
            document_list = []
            pass
        document_list.append(document)
        self.etypes[entity_type] = document_list

if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='XML Analyser')

    parser.add_argument('--harvest', required=True, dest='harvest', 
        help='The path to the harvested data to be transformed and submitted to the aggregate.')

    parser.add_argument('--map-by-selector', action="store_true", dest='mapbyselector', 
        help='Determine the entity type by an XPATH entity_type on the document.')
    parser.add_argument('--namespace', dest='namespace', 
        help='When using --map-by-selector, you need to provide the namespace for the XPATH selector. \
        Something like: "doc:::urn:isbn:1-931666-33-4"; where the part before the first ":::" \
        is the prefix and the rest is the actual namespace.')
    parser.add_argument('--entity', dest='entity_type', 
        help='When using --map-by-selector, you need to provide the XPATH entity_type for the entity type \
        element inside the document. Something like: "/doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType". \
        Note the prefix "doc" we defined in the --namespace argument.')
    parser.add_argument('--attribute', dest='attrib', 
        help="If the type is contained in an attribute, define that attribute here.")

    parser.add_argument('--nexamples', dest='nexamples', default='5', 
        help='How many example documents of each entity type to output = default 5.')

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # get the logger
    log = logging.getLogger('ANALYSER')

    if args.mapbyselector:
        if not args.namespace or not args.entity_type:
            log.error("When using --map-by-type, you also need to specify --namespace, --entity_type. See --help.")
            sys.exit()
        a = Analyser(args.nexamples, namespace=args.namespace, entity_type=args.entity_type, entity_attrib=args.attrib)
    else:
        ## if map by type is not specified
        log.debug("Assuming mappings are determined by the structure of the filename.")
        a = Analyser(args.nexamples)

    a.process(args.harvest)
