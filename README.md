# Site XSL Transforms

The site specific transforms. In here you'll find transforms
per entity type being harvested from the partner sites. See the harvester section
for how these are used / mapped / called.

## Dependencies

  - python-requests: http://docs.python-requests.org/en/latest/
  - python-lxml
  - python-argparse

## Test scripts

Scripts named as 'transform-tests.sh' in each of the site transform directories
are helpers for quickly testing that the transforms are working as expected.

Invoke as:

    ./transform-tests.sh

## Running the transformer - look at the examples in the test scripts

There are a bunch of helpers in this folder also.
