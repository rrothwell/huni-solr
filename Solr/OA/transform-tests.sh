#!/bin/bash

# source the configuration
. /etc/default/huni

site="OA"
entities="13327 900 15476 13921 780"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-oa.anu.edu.au-${e} --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity //doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id //doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done
