#!/bin/bash

# source the configuration
. /etc/default/huni

site="ParadisecA"
entities="LS1 DLGP1 JZ1"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-paradisec.org.au-${e} --transforms $TRANSFORMS/$site --site Paradisec \
        --map-by-selector --namespace r:::http://ands.org.au/standards/rif-cs/registryObjects \
        --entity //r:registryObjects/r:registryObject/r:collection --attrib 'type' \
        #--url http://localhost:8080/solr/dev/update \
        --debug                         
    echo -e "\n*******************************************\n"
done

site="ParadisecB"
entities="YT1-111D20050206 HDF1-YY20 TC9-004"
for e in $entities ; do
    transform.py --document $HARVEST/${site}/oai-paradisec.org.au-$e --transforms $TRANSFORMS/$site --site Paradisec  \
        --map-by-selector --namespace r:::http://www.openarchives.org/OAI/2.0/oai_dc/ --entity item \
        --debug
done
