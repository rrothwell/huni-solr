<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:olac="http://www.language-archives.org/OLAC/1.1/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:dcterms="http://purl.org/dc/terms/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
      <xsl:variable name="root" select="//olac:olac" />
      <add>
        <doc>
          <field name="type">Artefact</field>
          <field name="docid">
            <xsl:text>PARADISEC_item_</xsl:text>
            <xsl:value-of select="str:tokenize($root/dc:identifier)" />
          </field>
          <field name="prov_source"><xsl:value-of select="$root/dc:identifier[@xsi:type='dcterms:URI']" /></field>
          <field name="prov_site_short">PARADISEC</field>
          <field name="prov_site_address">http://catalog.paradisec.org.au</field>
          <field name="prov_site_long">PARADISEC</field>
          <field name="prov_site_tag">
            PARADISEC curates digital material about small or endangered languages.
          </field>
          <field name="title"><xsl:value-of select="$root/dc:title" /></field>
          <field name="description"><xsl:value-of select="$root/dc:description" /></field>
          <field name="physdesc"><xsl:value-of select="$root/dc:format" /></field>
        </doc>
      </add>  
  </xsl:template>
</xsl:stylesheet>



