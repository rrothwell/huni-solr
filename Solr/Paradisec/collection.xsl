<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:n="http://ands.org.au/standards/rif-cs/registryObjects"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <xsl:variable name="collection" select="//n:registryObjects/n:registryObject/n:collection" />
    <xsl:if test="$collection != ''">
        <xsl:call-template name="collection" />
    </xsl:if>
    <xsl:variable name="person" select="//n:registryObjects/n:registryObject/n:party[@type='person']" />
    <xsl:if test="$person != ''">
        <xsl:call-template name="people" />
    </xsl:if>
    <xsl:variable name="group" select="//n:registryObjects/n:registryObject/n:party[@type='group']" />
    <xsl:if test="$group != ''">
        <xsl:call-template name="group" />
    </xsl:if>
  </add>
  </xsl:template>

  <xsl:template name='people'>
    <xsl:variable name="root" select="//n:registryObjects/n:registryObject/n:party[@type='person']" />
    <doc>
      <field name="type">Person</field>
      <field name="docid">
        <xsl:text>PARADISEC_person_</xsl:text>
        <xsl:variable name="id" select="str:tokenize($root/n:identifier, '/')" />
        <xsl:value-of select="$id[last()]" />
      </field>
      <field name="prov_source"><xsl:value-of select="$root/n:identifier" /></field>
      <field name="prov_site_short">PARADISEC</field>
      <field name="prov_site_address">http://catalog.paradisec.org.au</field>
      <field name="prov_site_long">PARADISEC</field>
      <field name="prov_site_tag">
        PARADISEC curates digital material about small or endangered languages.
      </field>
      <field name="given_name"><xsl:value-of select="$root/n:name/n:namePart[@type='given']" /></field>
      <field name="family_name"><xsl:value-of select="$root/n:name/n:namePart[@type='family']" /></field>
    </doc>
  </xsl:template>

  <xsl:template name='group'>
    <xsl:variable name="root" select="//n:registryObjects/n:registryObject/n:party[@type='group']" />
    <doc>
      <field name="type">Organisation</field>
      <field name="docid">
        <xsl:text>PARADISEC_organisation_</xsl:text>
        <xsl:variable name="id" select="str:tokenize($root/n:identifier, '/')" />
        <xsl:value-of select="$id[last()]" />
      </field>
      <field name="prov_source"><xsl:value-of select="$root/n:identifier" /></field>
      <field name="prov_site_short">PARADISEC</field>
      <field name="prov_site_address">http://catalog.paradisec.org.au</field>
      <field name="prov_site_long">PARADISEC</field>
      <field name="prov_site_tag">
        PARADISEC curates digital material about small or endangered languages.
      </field>
      <field name="name"><xsl:value-of select="$root/n:name/n:namePart[@type='primary']" /></field>
    </doc>
  </xsl:template>


  <xsl:template name='collection'>
    <xsl:variable name="root" select="//n:registryObjects/n:registryObject/n:collection" />
    <doc>
      <field name="type">Collection</field>
      <field name="docid">
      <xsl:text>PARADISEC_collection_</xsl:text> 
      <xsl:variable name="id" select="str:tokenize($root/n:identifier, '/')" />
      <xsl:value-of select="$id[last()]" />
      </field>
      <field name="prov_source"><xsl:value-of select="$root/n:identifier" /></field>
      <field name="prov_site_short">PARADISEC</field>
      <field name="prov_site_address">http://catalog.paradisec.org.au</field>
      <field name="prov_site_long">PARADISEC</field>
      <field name="prov_site_tag">
        PARADISEC curates digital material about small or endangered languages.
      </field>
      <field name="title"><xsl:value-of select="$root/n:name/n:namePart" /></field>
      <field name="description"><xsl:value-of select="$root/n:description" /></field>
    </doc>
  </xsl:template>


</xsl:stylesheet>



