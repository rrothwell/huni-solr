#!/bin/bash

# source the configuration
. /etc/default/huni

echo "Analysing Paradisec collections"
SITE="ParadisecA"
analyse.py --harvest $HARVEST/$SITE --map-by-selector --namespace r:::http://ands.org.au/standards/rif-cs/registryObjects \
    --entity //r:registryObjects/r:registryObject/r:collection --attrib 'type'

#echo "Analysing Paradisec items"
#SITE="ParadisecB"
#analyse.py --harvest $HARVEST/$SITE --map-by-selector --namespace r:::http://www.openarchives.org/OAI/2.0/oai_dc/ \
#    --entity '//r:olac/{http://purl.org/dc/elements/1.1/}:type' --attrib 'xsi:type'
