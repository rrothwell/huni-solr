# Field types in use

The fields found in the XSLTs (not every field is in every XSLT)


## OHRMs
    docid: text (string)
    type: text (text)
    source: url (text)
    abstract: text (text_en_splitting)
    name: text (text_en_splitting)
    family_name: text (text_en_splitting, copyfield text)
    given_name: text (text_en_splitting, copyfield text)
    date_start: date (date)
    date_end: date (date)
    date_birth: date (date)
    date_death: date (date)
    occupation: text (text_en_splitting, copyfield text)