#!/bin/bash

# source the configuration
. /etc/default/huni

site="EOAS"
entities="P004862 A001045 P005074 P001452 A000399"
for e in $entities ; do
    transform.py --document $HARVEST/${site}/oai-eoasnew.oai.esrc.unimelb.edu.au-${e} --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity /doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id /doc:eac-cpf/doc:control/doc:recordId \
        --debug
    echo -e "\n*******************************************\n"
done
