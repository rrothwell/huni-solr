<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:inm="http://www.inmagic.com/webpublisher/query"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Artefact</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">AFIRC</field>
      <field name="prov_site_address">http://www.afiresearch.rmit.edu.au/</field>
      <field name="prov_site_long">AFI Research Collection</field>
      <field name="prov_site_tag">A specialist film and television industry resource.</field>
      <field name="title"><xsl:call-template name="title"/></field>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="description"><xsl:call-template name="notes"/></field>
      <field name="physdesc"><xsl:call-template name="physdesc"/></field>
      <field name="subject"><xsl:call-template name="subject"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://afidb.adc.rmit.edu.au/dbtw-wpd/catalogues.htm</xsl:text>
  </xsl:template>

  <xsl:template name='title'>
    <xsl:value-of select="/inm:Record/inm:Title" /> 
  </xsl:template>

  <xsl:template name="subtitle">
    <xsl:value-of select="/inm:Record/inm:Subtitle" />
  </xsl:template>

  <xsl:template name='description'>
    <xsl:value-of select="/inm:Record/inm:Summary" /> 
  </xsl:template>

  <xsl:template name='notes'>
    <xsl:value-of select="/inm:Record/inm:Notes" /> 
  </xsl:template>

  <xsl:template name='physdesc'>
    <xsl:value-of select="/inm:Record/inm:PhysDes" /> 
  </xsl:template>

  <xsl:template name='subject'>
    <xsl:value-of select="/inm:Record/inm:Subject" /> 
  </xsl:template>
</xsl:stylesheet>



