#!/bin/bash

# source the configuration
. /etc/default/huni

site="AFIRC"
entities="Music_LP-57129 Clippings_file-52481 Script-58796 DVD-52669 DVD_Research_Folder-46003 \
Monograph_-_Levy_Collection-43409 Special_issue-21534 Poster-49626 Journal-56821 Annual_report-26940 \
Festival_catalogue-33409 Newspaper_Article-46130 Monograph-25264 Journal_article-54002 \
Serial-30681 Still-50341"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-afirc.rmit.edu.au-${e}.xml \
        --transforms $TRANSFORMS/$site --site ${site} \
        --debug                         
    echo -e "\n*******************************************\n"
done

