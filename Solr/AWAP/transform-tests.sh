#!/bin/bash

# source the configuration
. /etc/default/huni

site="AWAP"
entities="AWE0367 AWE2278 AWE4875 AWE0669 AWE2372 AWE0540 AWE4909 AWE0142"
for e in $entities ; do
    transform.py --document $HARVEST/${site}/oai-awapnew.oai.esrc.unimelb.edu.au-${e} --transforms $TRANSFORMS/$site --site ${site}\
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity //doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id //doc:eac-cpf/doc:control/doc:recordId \
        --debug
    echo -e "\n*******************************************\n"
done
