<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://caarp.deakin.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Film</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">CAARP</field>
      <field name="prov_site_address">http://caarp.flinders.edu.au/</field>
      <field name="prov_site_long">CAARP - Cinema and Audiences Research Project database</field>
      <field name="prov_site_tag">An online encyclopaedia of cinema-going in Australia.</field>
      <field name="title"><xsl:value-of select="/d:film/d:film_title/d:title"/></field>
      <field name="date_begin"><xsl:value-of select="/d:film/d:production_year"/></field>
      <field name="description"><xsl:value-of select="/d:film/d:description"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text disable-output-escaping="yes">http://caarp.flinders.edu.au/film/view/</xsl:text>
    <xsl:value-of select="/d:film/d:id" />
  </xsl:template>

</xsl:stylesheet>



