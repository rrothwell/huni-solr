#!/bin/bash

# source the configuration
. /etc/default/huni

site="CAARP"
entities="company-952 venue-2047 film-1845 film-3000 film-48 film-10 venue-300 venue-448"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-3Acaarp.deakin.edu.au-${e}.xml \
        --transforms $TRANSFORMS/$site --site ${site} \
        --debug                         
    echo -e "\n*******************************************\n"
done

