<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://caarp.deakin.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Venue</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">CAARP</field>
      <field name="prov_site_address">http://caarp.flinders.edu.au/</field>
      <field name="prov_site_long">CAARP - Cinema and Audiences Research Project database</field>
      <field name="prov_site_tag">An online encyclopaedia of cinema-going in Australia.</field>
      <field name="name"><xsl:value-of select="/d:venue/d:name"/></field>
      <field name="address"><xsl:value-of select="/d:venue/d:venue_attributes/d:attribute_value[following-sibling::d:attribute_key = 'address']"/></field>
      <field name="latitude"><xsl:value-of select="/d:venue/d:venue_attributes/d:attribute_value[following-sibling::d:attribute_key = 'latitude']"/></field>
      <field name="longitude"><xsl:value-of select="/d:venue/d:venue_attributes/d:attribute_value[following-sibling::d:attribute_key = 'longitude']"/></field>
      <field name="suburb"><xsl:value-of select="/d:venue/d:venue_attributes/d:attribute_value[following-sibling::d:attribute_key = 'suburb']"/></field>
      <field name="date_begin"><xsl:value-of select="/d:venue/d:venue_operation_dates/d:from_date"/></field>
      <field name="date_end"><xsl:value-of select="/d:venue/d:venue_operation_dates/d:to_date"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text disable-output-escaping="yes">http://caarp.flinders.edu.au/venue/view/</xsl:text>
    <xsl:value-of select="/d:venue/d:id" />
  </xsl:template>

</xsl:stylesheet>



