#!/bin/bash

# source the configuration
. /etc/default/huni

SITE="FCNS"
analyse.py --harvest $HARVEST/$SITE --map-by-selector --namespace r:::urn:isbn:1-931666-33-4 \
    --entity //r:eac-cpf/r:cpfDescription/r:identity/r:entityType --info