#!/bin/bash

# source the configuration
. /etc/default/huni

site="FCQD"
entities="QE00360 QE00425 QE00474 QE00326"
for e in $entities ; do
    transform.py --document $HARVEST/${site}/${e}.xml --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity /doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id /doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done   
