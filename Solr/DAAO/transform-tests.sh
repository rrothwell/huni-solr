#!/bin/bash

# source the configuration
. /etc/default/huni

site="DAAO"
entities="bio_group_kodak-australasia-pty-ltd bio_group_warlpiri-media bio_group_jam-factory-craft-design-centre-adelaide-sa \
bio_frances-lindsay bio_ann-elizabeth-turner bio_john-michael-loughnan"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-daao.org.au-${e} --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity //doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id //doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done
