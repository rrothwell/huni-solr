#!/bin/bash

# source the configuration
. /etc/default/huni

site="FCVC"
entities="E000683 E000255 E000429 E000577 E000676"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/${e}.xml --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity /doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id /doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done
