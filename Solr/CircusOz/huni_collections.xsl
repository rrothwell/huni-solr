<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://circusozdev.eres.rmit.edu.au/hunni/feed"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Collection</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="name"><xsl:value-of select="/d:venue/d:name"/></field>
      <field name="prov_site_short">CircusOz</field>
      <field name="prov_site_address">http:/archive.circusoz.com</field>
      <field name="prov_site_long">CircusOz</field>
      <field name="prov_site_tag">CircusOz video archive.</field>
      <field name="title"><xsl:value-of select="/d:huni_collections/d:collection_title" /></field>
      <field name="description"><xsl:value-of select="/d:huni_collections/d:collection_description" /></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://archive.circusoz.com/collections/view/</xsl:text>
    <xsl:value-of select="/d:huni_collections/d:collection_id" />
  </xsl:template>

</xsl:stylesheet>



