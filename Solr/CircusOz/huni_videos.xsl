<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://circusozdev.eres.rmit.edu.au/hunni/feed"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Videos</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="name"><xsl:value-of select="/d:venue/d:name"/></field>
      <field name="prov_site_short">CircusOz</field>
      <field name="prov_site_address">http:/archive.circusoz.com</field>
      <field name="prov_site_long">CircusOz</field>
      <field name="prov_site_tag">CircusOz video archive.</field>
      <field name="name"><xsl:value-of select="/d:huni_videos/d:venue_name" /></field>
      <field name="title"><xsl:value-of select="/d:huni_videos/d:title" /></field>
      <field name="city"><xsl:value-of select="/d:huni_videos/d:city" /></field>
      <field name="date_begin"><xsl:value-of select="/d:huni_videos/d:start_date"/></field>
      <field name="date_end"><xsl:value-of select="/d:huni_videos/d:end_date"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://archive.circusoz.com/clips/view/</xsl:text>
    <xsl:value-of select="/d:huni_videos/d:id" />
  </xsl:template>

</xsl:stylesheet>



