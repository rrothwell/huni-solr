#!/bin/bash

# source the configuration
. /etc/default/huni

site="CircusOz"
entities="huni_collections-31 huni_collections-30 huni_acts-3961 huni_acts-4439 huni_videos-46 huni_videos-79 huni_performers-1030 huni_performers-129"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-archive.circusoz.com-${e}.xml \
        --transforms $TRANSFORMS/$site --site ${site} \
        --debug                         
    echo -e "\n*******************************************\n"
done

