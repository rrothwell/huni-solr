#!/bin/bash

# source the configuration
. /etc/default/huni

site="EMEL"
entities="EM02105 EM02070 EM02949 EM03104 EM01938 EM00977 EM00648 EM01473 EM02149"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-emel.oai.esrc.unimelb.edu.au-${e} --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity //doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id //doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done
