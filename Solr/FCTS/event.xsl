<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="urn:isbn:1-931666-33-4"
    xmlns:relation="urn:isbn:1-931666-22-9"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Event</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">FCTS</field>
      <field name="prov_site_address">http://www.findandconnect.gov.au/tas</field>
      <field name="prov_site_long">Find and Connect Tas</field>
      <field name="prov_site_tag">The Find and Connect web resource is for Forgotten Australians, Former Child Migrants and
      everyone with an interest in the history of out-of-home 'care' in Australia.
      </field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="date_begin"><xsl:call-template name="date_start"/></field>
      <field name="date_end"><xsl:call-template name="date_end"/></field>
      <field name="description"><xsl:call-template name="abstract"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:identity/d:entityId"/>
  </xsl:template>

  <xsl:template name='name'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:identity/d:nameEntry/d:part" /> 
  </xsl:template>

  <xsl:template name='date_start'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:existDates/d:dateRange/d:fromDate/@standardDate" /> 
  </xsl:template>

  <xsl:template name='date_end'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:existDates/d:dateRange/d:toDate/@standardDate" /> 
  </xsl:template>

  <xsl:template name='abstract'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:biogHist/d:abstract" /> 
  </xsl:template>
</xsl:stylesheet>



