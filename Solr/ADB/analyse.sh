#!/bin/bash

# source the configuration
. /etc/default/huni

SITE="ADB"
analyse.py --info --harvest $HARVEST/$SITE \
     --map-by-selector \
     --namespace r:::urn:isbn:1-931666-33-4 --entity //r:eac-cpf/r:cpfDescription/r:identity/r:entityType