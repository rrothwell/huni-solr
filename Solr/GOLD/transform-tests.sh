#!/bin/bash

# source the configuration
. /etc/default/huni

site="GOLD"
entities="EG00077 EG00056 EG00077 EG00318 EG00267 EG00205"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-gold.oai.esrc.unimelb.edu.au-${e} --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity //doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id //doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done
