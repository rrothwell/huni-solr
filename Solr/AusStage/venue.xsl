<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Venue</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="name"><xsl:value-of select="/d:venue/d:name"/></field>
      <field name="prov_site_short">AusStage</field>
      <field name="prov_site_address">http://www.ausstage.edu.au</field>
      <field name="prov_site_long">AusStage</field>
      <field name="prov_site_tag">Researching Australian Live Performance.</field>
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="address"><xsl:value-of select="/d:venue/d:STREET"/></field>
      <field name="latitude"><xsl:value-of select="/d:venue/d:LATITUDE"/></field>
      <field name="longitude"><xsl:value-of select="/d:venue/d:LONGITUDE"/></field>
      <field name="suburb"><xsl:value-of select="/d:venue/d:SUBURB"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://www.ausstage.edu.au/pages/venue/</xsl:text>
    <xsl:value-of select="/d:venue/d:VENUEID" />
  </xsl:template>

  <xsl:template name='name'>
    <xsl:value-of select="/d:venue/d:VENUE_NAME" />
  </xsl:template>

</xsl:stylesheet>



