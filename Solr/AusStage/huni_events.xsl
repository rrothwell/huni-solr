<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Event</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">AusStage</field>
      <field name="prov_site_address">http://www.ausstage.edu.au</field>
      <field name="prov_site_long">AusStage</field>
      <field name="prov_site_tag">Researching Australian Live Performance.</field> 
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="date_begin"><xsl:call-template name="date_start"/></field>
      <field name="date_end"><xsl:call-template name="date_end"/></field>
      <field name="description"><xsl:call-template name="description"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://www.ausstage.edu.au/pages/event/</xsl:text>
    <xsl:value-of select="/d:events/d:EVENTID" />
  </xsl:template>

  <xsl:template name='name'>
    <xsl:value-of select="/d:events/d:EVENT_NAME" />
  </xsl:template>

  <xsl:template name='date_start'>
    <xsl:value-of select="/d:events/d:YYYYFIRST_DATE" />
  </xsl:template>

  <xsl:template name='date_end'>
    <xsl:value-of select="/d:events/d:YYYYLAST_DATE" />
  </xsl:template>

  <xsl:template name='description'>
    <xsl:value-of select="/d:events/d:description" />
  </xsl:template>
</xsl:stylesheet>



