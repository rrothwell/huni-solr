<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Artefact</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">AusStage</field>
      <field name="prov_site_address">http://www.ausstage.edu.au</field>
      <field name="prov_site_long">AusStage</field>
      <field name="prov_site_tag">Researching Australian Live Performance.</field> 
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="description"><xsl:call-template name="description"/></field>
      <field name="description"><xsl:call-template name="detail_comments"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://www.ausstage.edu.au/pages/resource/</xsl:text>
    <xsl:value-of select="/d:item/d:ITEMID" />
  </xsl:template>

  <xsl:template name='name'>
    <xsl:value-of select="/d:item/d:CITATION" />
  </xsl:template>

  <xsl:template name='detail_comments'>
    <xsl:value-of select="/d:item/d:DETAIL_COMMENTS" />
  </xsl:template>

  <xsl:template name='description'>
    <xsl:value-of select="/d:item/d:ITEM_DESCRIPTION" />
  </xsl:template>
</xsl:stylesheet>



