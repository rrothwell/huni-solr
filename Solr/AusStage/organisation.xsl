<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Organisation</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">AusStage</field>
      <field name="prov_site_address">http://www.ausstage.edu.au</field>
      <field name="prov_site_long">AusStage</field>
      <field name="prov_site_tag">Researching Australian Live Performance.</field> 
      <field name="name"><xsl:call-template name="name"/></field>
      <field name="address"><xsl:call-template name="address"/></field>
      <field name="suburb"><xsl:call-template name="suburb"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://www.ausstage.edu.au/pages/organisation/</xsl:text>
    <xsl:value-of select="/d:organisation/d:ORGANISATIONID" />
  </xsl:template>

  <xsl:template name='name'>
    <xsl:value-of select="/d:organisation/d:NAME" />
  </xsl:template>

  <xsl:template name='address'>
    <xsl:value-of select="/d:organisation/d:ADDRESS" />
  </xsl:template>

  <xsl:template name='suburb'>
    <xsl:value-of select="/d:organisation/d:SUBURB" />
  </xsl:template>

  <xsl:template name='description'>
    <xsl:value-of select="/d:organisation/d:notes" />
  </xsl:template>
</xsl:stylesheet>



