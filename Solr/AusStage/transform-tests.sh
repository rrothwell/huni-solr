#!/bin/bash

# source the configuration
. /etc/default/huni

site="AusStage"
entities="item-35469 item-335 organisation-11791 organisation-33935 venue-13786 venue-10638 huni_events-80807 huni_events-83857 
huni_contributors-241527 huni_contributors-456551"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-ausstage.edu.au-${e}.xml \
        --transforms $TRANSFORMS/$site --site ${site} \
        --debug                         
    echo -e "\n*******************************************\n"
done

