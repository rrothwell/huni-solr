<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://www.ausstage.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Person</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="name"><xsl:value-of select="/d:venue/d:name"/></field>
      <field name="prov_site_short">AusStage</field>
      <field name="prov_site_address">http://www.ausstage.edu.au</field>
      <field name="prov_site_long">AusStage</field>
      <field name="prov_site_tag">Researching Australian Live Performance.</field>
      <field name="given_name"><xsl:value-of select="/d:huni_contributors/d:first_name"/></field>
      <field name="family_name"><xsl:value-of select="/d:huni_contributors/d:last_name"/></field>
      <field name="date_begin"><xsl:value-of select="/d:huni_contributors/d:date_of_birth"/></field>
      <field name="date_end"><xsl:value-of select="/d:huni_contributors/d:date_of_death"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text>http://www.ausstage.edu.au/pages/contributor/</xsl:text>
    <xsl:value-of select="/d:huni_contributors/d:contributorid" />
  </xsl:template>

</xsl:stylesheet>



