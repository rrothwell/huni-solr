#!/bin/bash

# source the configuration
. /etc/default/huni

site="FCAC"
entities="AE00011 AE00024 AE00116 AE00042 AE00083"
for e in $entities ; do
    transform.py --document $HARVEST/${site}/${e}.xml --transforms $TRANSFORMS/$site --site ${site} \
        --map-by-selector --namespace doc:::urn:isbn:1-931666-33-4 \
        --entity /doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType \
        --entity_id /doc:eac-cpf/doc:control/doc:recordId \
        --debug                         
    echo -e "\n*******************************************\n"
done    
