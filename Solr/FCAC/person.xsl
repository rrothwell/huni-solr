<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="urn:isbn:1-931666-33-4"
    xmlns:relation="urn:isbn:1-931666-22-9"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Person</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">FCAC</field>
      <field name="prov_site_address">http://www.findandconnect.gov.au/act</field>
      <field name="prov_site_long">Find and Connect ACT</field>
      <field name="prov_site_tag">The Find and Connect web resource is for Forgotten Australians, Former Child Migrants and
      everyone with an interest in the history of out-of-home 'care' in Australia.
      </field>
      <field name="family_name"><xsl:call-template name="family_name"/></field>
      <field name="given_name"><xsl:call-template name="given_name"/></field>
      <field name="date_begin"><xsl:call-template name="date_birth"/></field>
      <field name="date_end"><xsl:call-template name="date_death"/></field>
      <field name="occupation"><xsl:call-template name="occupation"/></field>
      <field name="bio"><xsl:call-template name="abstract"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:identity/d:entityId"/>
  </xsl:template>

  <xsl:template name='family_name'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:identity/d:nameEntry/d:part[@localType='familyname']" /> 
  </xsl:template>

  <xsl:template name='given_name'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:identity/d:nameEntry/d:part[@localType='givenname']" /> 
  </xsl:template>
  
  <xsl:template name='date_birth'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:existDates/d:dateRange/d:fromDate/@standardDate" /> 
  </xsl:template>

  <xsl:template name='date_death'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:existDates/d:dateRange/d:toDate/@standardDate" /> 
  </xsl:template>

  <!-- need place of birth and death -->

  <xsl:template name='occupation'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:occupations/d:occupation/d:term" />
  </xsl:template>

  <xsl:template name='abstract'>
    <xsl:value-of select="/d:eac-cpf/d:cpfDescription/d:description/d:biogHist/d:abstract" /> 
  </xsl:template>
</xsl:stylesheet>



