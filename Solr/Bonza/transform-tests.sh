#!/bin/bash

# source the configuration
. /etc/default/huni

site="Bonza"
entities="bibliography-2196 bibliography-12065 company-1864 person-7421 production-1845"
for e in $entities ; do 
    transform.py --document $HARVEST/${site}/oai-bonza.deakin.edu.au-${e}.xml \
        --transforms $TRANSFORMS/$site --site ${site} \
        --debug                         
    echo -e "\n*******************************************\n"
done

