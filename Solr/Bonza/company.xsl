<?xml version="1.0"?>
<!-- http://stackoverflow.com/questions/284094/how-to-select-from-xml-with-namespaces-with-xslt -->

<xsl:stylesheet version="1.0" 
    xmlns:d="http://bonza.deakin.edu.au/namespace"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    >

  <xsl:output method="xml" encoding="UTF-8" indent="yes" />

  <xsl:template match="/">
  <add>
    <doc>
      <field name="type">Organisation</field>
      <field name="prov_source"><xsl:call-template name="source"/></field>
      <field name="prov_site_short">Bonza</field>
      <field name="prov_site_address">http://www.bonzadb.com.au/</field>
      <field name="prov_site_long">BONZA - National Cinema and Television Database</field>
      <field name="prov_site_tag">An online collection of national cinema databases (to date French, Australian and New Zealand) 
      prepared by film researchers and students.
      </field>
      <field name="name"><xsl:value-of select="/d:company/d:name"/></field>
    </doc>
  </add>
  </xsl:template>

  <xsl:template name='source'>
    <xsl:text disable-output-escaping="yes">http://www.bonzadb.com.au/company/view/</xsl:text>
    <xsl:value-of select="/d:company/d:id" />
  </xsl:template>

</xsl:stylesheet>



