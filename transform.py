#!/usr/bin/env python

from lxml import etree
import sys
import os.path
import argparse
import logging
import ConfigParser
import requests
from datetime import date
from datetime import datetime

class Index:
    """All the required to manage submission to Solr"""
    def __init__(self, update_url):
        self.update_url = update_url
        self.headers = { 'Content-type': 'text/xml; charset=utf-8' }

    def commit(self):
        """Commit the pending updates"""
        msg = '<commit expungeDeletes="true"/>'
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully committed the changes.")
        else:
            log.error("Something went wrong trying to commit the changes.")
            log.error("\n%s" % resp.text)

    def clean(self):
        """Delete all documents that don't have the current version"""
        msg = "<delete><query>*:*</query></delete>"
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully submitted the index delete crequest.")
        else:
            log.error("Something went wrong trying to submit a request to wipe the index.")
            log.error("\n%s" % resp.text)

    def optimize(self):
        """Optimize the on disk index"""
        msg = '<optimize waitSearcher="false"/>'
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully optimized the index.")
        else:
            log.error("Something went wrong trying to optimize the index.")
            log.error("\n%s" % resp.text)

    def submit(self, doc, document_name):
        """Submit the document for indexing"""
        resp = requests.post(self.update_url, data=doc, headers=self.headers) 
        if resp.status_code == 200:
            log.debug("%s successfully submitted for indexing." % document_name)
        else:
            log.error("Submission of %s failed with error %s." % (document_name, resp.status_code))

class counter:
    """A counter for outputting progress in --info mode"""
    def __init__(self, total):
        self.last = 0
        self.total = total
        if total < 1000:
            self.logat = 100
        elif total < 100000:
            self.logat = 1000
        else:
            self.logat = 10000
    
    def update(self, current):
        if current > (self.last + self.logat):
            self.last += self.logat
            try:
                process_time = time.time() - self.last_time
                log.info("Processed %s of %s... (%s records in %1.2f seconds)" % (self.last, self.total, self.logat, float(process_time)))
            except:
                log.info("Processed %s of %s..." % (self.last, self.total))

class date_cleanser:
    def __init__(self, date_fields):
        # time formats we will try to clean
        self.timeformats = [
            "%Y-%m-%d", # 1976-01-01
            "%Y %m %d", # 1976 01 01
            "%d %B %Y", # 12 January 1997
            "%B %Y",    # February 1998
            "%Y",       # 2004
            "c. %Y",    # c. 2004
            "%Y?",      # 2004?
        ]

        # the list of date fields in use
        self.date_fields = date_fields

    def clean(self, datevalue):
        """Date data needs to be in Solr date format

        The format for this date field is of the form 1995-12-31T23:59:59Z,

        @params:
        date: the date string we want to standardise
        """
        #  handle whatever it is we find...
        for timeformat in self.timeformats:
            try:
                datevalue = datetime.strptime(datevalue, timeformat)
                datevalue = "%sZ" % str(datevalue).replace(' ', 'T')
                break
            except:
                pass
                # ie try the next format until we succeed, or get to the end of the list

        # here we check that we can read the datevalue using
        #  the expected format. If there wasn't a suitable format in the list,
        #  then here we'll find the original string, it will fail, and a warning
        #  will be issued.
        try:
            checkdate = datetime.strptime(datevalue, "%Y-%m-%dT%H:%M:%SZ")
            return datevalue
        except ValueError as e:
            #log.warn("Unknown time format: %s" % datevalue)
            return None


class Transform:
    def __init__(self, site, transforms, index=None, namespace=None, entity_type=None, entity_attrib=None, entity_id=None):
        # store the location of the transforms base where we'll go looking
        #  for the required entity transform
        self.transforms = transforms
        
        # store the URL of the index we're submitting to
        self.index = index

        # the site we're processing - used in the creation of the document ID
        self.site = site

        # the list of date fields in use
        self.date_fields = [ 'date_begin', 'date_end' ]

        if namespace is not None and entity_type is not None:
            try:
                self.namespace = { namespace.split(':::')[0]: namespace.split(':::')[1] }
            except IndexError:
                log.error('Is your namespace in the correct form: {prefix}:::{namespace declaration}')
                log.error('Note the ":::" delimiting the prefix and namespace')
                sys.exit()
            self.entity_type = entity_type
            self.entity_id = entity_id
            self.entity_attribute = entity_attrib

        else:
            self.namespace = None
            self.entity_type = None
            self.entity_id = None

    def check_transform(self, transform):
        """Check that the transform form file exists"""
        if not os.path.exists(transform):
            log.error("Can't find %s so I'm unable to transform that document." % transform)
            return None

    def process_path(self, path):
        """Iterate over a directory of harvested datafiles
        
        @params:
        path: the directory of content to be transformed and ingested
        """
        index = Index(self.index)
        for (dirpath, dirnames, filenames) in os.walk(path):
            total = len(filenames)
            c = counter(total)
            i = 0
            for fname in filenames:
                i += 1
                c.update(i)
                self.process_document("%s/%s" % (dirpath, fname))

        # we're done! commit and optimize
        index.commit()
        index.optimize()

    def process_document(self, document):
        """Process a single datafile

        @params:
        document: the file to be processed
        """
        if self.namespace is not None:
            try:
                doc = etree.parse(document)
                if self.entity_attribute is not None:
                    # if an attribute is defined then we can assume an XPATH has also been passed in
                    #  otherwise what's the point!
                    entity_type = doc.xpath(self.entity_type, namespaces=self.namespace)[0].attrib[self.entity_attribute]
                else:
                    # this could be an XPATH or a defined type
                    #  if it begins with a slash - consider it an XPATH
                    #  otherwise use it as a defined type
                    if self.entity_type.startswith('/'):
                        entity_type = doc.xpath(self.entity_type, namespaces=self.namespace)[0].text
                    else:
                        entity_type = self.entity_type

                if self.entity_id is not None:
                    entity_id = doc.xpath(self.entity_id, namespaces=self.namespace)[0].text
                    identity = "%s_%s_%s" % (self.site, entity_type, entity_id)
                else:
                    identity = None
                transform = "%s/%s.xsl" % (self.transforms, entity_type)
            except IndexError:
                log.error("Couldn't determine entity type of %s, skipping it for now." % document)
                return
            except etree.XMLSyntaxError:
                log.error("Screwey document; check it! %s" % document)
                return
            except IOError:
                log.error("No such file: %s" % document)
                return

        else: 
            try:
                entity_type = document.split('-')[2]
                entity_id = document.split('-')[3]
                entity_id = entity_id.split('.xml')[0]
                transform = "%s/%s.xsl" % (self.transforms, entity_type)
                identity = "%s_%s_%s" % (self.site, entity_type, entity_id)
            except IndexError:
                log.error("I can't seem to get the entity type from %s, skipping it for now." % document)
                return None

        if not os.path.exists(transform):
            log.error("Can't find %s so I'm unable to transform that document." % transform)
        else:
            #identity = "%s_%s_%s" % (self.site, entity_type, entity_id)
            log.debug("Using %s to transform %s; document id: %s" % (transform, document, identity))
            if not self.check_transform(transform):
                self.doit(document, transform, identity)

    def clean_dates(self, doc):
        """Date data needs to be in Solr date format

        The format for this date field is of the form 1995-12-31T23:59:59Z,

        @params:
        doc: the XML document
        """
        date_elements = [ e for e in doc.iter() if e.get('name') in self.date_fields ]
        for e in date_elements:
            # have we found an empty or missing date field ?
            if e.text is None:
                continue

            dc = date_cleanser(self.date_fields)
            datevalue = dc.clean(e.text)
            e.text = datevalue

    def strip_empty_elements(self, doc):
        """Remove empty elements from the document.

        Solr date fields don't like to be empty - hence why this
        method exists. As it turns out, it can't hurt to ditch empty
        elements - less to submit. Hence why it's generic

        @params:
        doc: the XML document
        """
        for elem in doc.iter('field'):
            if elem.text is None:
                elem.getparent().remove(elem)
        
    def doit(self, document, transform, identity):
        """Transform the document, clean it, submit it

        This method:
         - parses an XML data file, 
         - transforms it,
         - fixes up the known date fields,
         - removes empty elements,
         - submits the document for indexing

        @params:
        document: the XML file to be transformed
        transform: the XSL transform to use against it
        identity: the value to be used for the document uniqueid
        """
        # read in the XSL transform 
        try:
            xslt = etree.parse(transform)
        except etree.XMLSyntaxError:
            log.error("Screwey transform; check it! %s" % transform)
            return

        # handle any includes
        xslt.xinclude()

        # read the document to be transformed
        doc = etree.parse(document)

        # transform it
        #  we'll assume the transform will actually load
        #  so we won't bother with exceptions
        transform = etree.XSLT(xslt)
        doc = transform(doc)

        # get the root of the document
        tmp = doc.xpath('/add/doc')[0]

        # add the id field if not None
        if identity is not None:
            docid = etree.Element('field', name='docid')
            docid.text = identity
            tmp.append(docid)

        # update the last ingest time (the assumption here is that
        #  the transform happens shortly after harvest)
        doc_lastupdate = etree.Element('field', name="prov_doc_last_update")
        doc_lastupdate.text = datetime.strftime(datetime.now(), "%Y-%m-%dT%H:00:00Z")
        tmp.append(doc_lastupdate)

        # clean the date entries for solr
        self.clean_dates(doc)

        # strip empty elements - dates in particular cause
        #  solr to barf horribly...
        self.strip_empty_elements(doc)

        # submit the document to solr
        if self.index is not None:
            index = Index(self.index)
            index.submit(etree.tostring(doc), document)

        # write to stdout
        log.debug("\n" + etree.tostring(doc, pretty_print=True))


if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='XSL transformer')
    parser.add_argument('--site', required=True, dest='site', 
        help='The codename for the site; e.g. WALL or Bonza.')

    parser.add_argument('--harvest', dest='harvest', 
        help='The path to the harvested data to be transformed and submitted to the aggregate.')
    parser.add_argument('--document', dest='document', help='A single document to process - useful for testing')

    parser.add_argument('--transforms', required=True, dest='transforms', 
        help='The path to the transforms to be used with this data.')

    parser.add_argument('--url', dest='index', 
        help='The URL to POST the output document to. Ensure you submit the document to the appropriate \
            index: ie if you\'re creating a solr doc, send it to solr not the RDF triple store. \
            If not defined, the post step won\'t happen. Useful with --debug to spit out the doc \
            without going near the index.')

    parser.add_argument('--map-by-selector', action="store_true", dest='mapbyselector', 
        help='Determine the entity type by an XPATH selection on the document.')
    parser.add_argument('--namespace', dest='namespace', 
        help='When using --map-by-selector, you need to provide the namespace for the XPATH selector. \
        Something like: "doc:::urn:isbn:1-931666-33-4"; where the part before the first ":::" \
        is the prefix and the rest is the actual namespace.')
    parser.add_argument('--entity', dest='entity_type', 
        help='When using --map-by-selector you need to provide either the XPATH selector for the entity type \
        element inside the document or the entity type itself (assuming all of the content is of the same type). \
        If going the XPATH route then you need to provide something like: "/doc:eac-cpf/doc:cpfDescription/doc:identity/doc:entityType". \
        (Note the prefix "doc" we defined in the --namespace argument.) If going the defined entity type route then just pass in a name; \
        e.g. item. The tool will use the presence or absence of a leading / (slash, path delineator) to determine what you\'re specifying. \
        That is, a leading slash will cause this to be treated as an XPATH selector whilst absence of a leading slash will cause \
        it to be treated as the item type.')
    parser.add_argument('--attribute', dest='attrib',
        help="If the type is contained in an attribute, define that attribute here.")
    parser.add_argument('--entity_id', dest='entity_id', 
        help='When using --map-by-selector, you need to provide the XPATH selector for the entity ID \
        inside the document. Something like: "/doc:eac-cpf/doc:control/doc:recordId". Note the prefix \
        "doc" we defined in the --namespace argument. If you don\'t provide use this option then it is up to you to \
        add the field type="docid" to the document and to ensure it\'s unique. You really should pass an \
        XPATH selector for the identifier if you can.')


    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')

    args = parser.parse_args()

    # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # quieten the requests library
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.ERROR)

    # get the logger
    log = logging.getLogger('TRANSFORMER')

    ## check we only get one of harvest and document
    if args.harvest and args.document:
        log.error("Specify only one of --harvest and --document")
        sys.exit()
    
    if not args.harvest and not args.document:
        log.error("Specify at least one of --harvest and --document")
        sys.exit()

    if args.mapbyselector:
        if not args.namespace or not args.entity_type:
            log.error("When using --map-by-selector, you also need to specify --namespace, --entity. See --help.")
            sys.exit()
        t = Transform(args.site, args.transforms, args.index,
                namespace=args.namespace, entity_type=args.entity_type, entity_attrib=args.attrib, entity_id=args.entity_id)
    else:
        ## if map by type is not specified
        log.debug("Assuming mappings are determined by the structure of the filename.")
        t = Transform(args.site, args.transforms, args.index)

    # HOORAY!! do it; finally.
    if args.document:
        t.process_document(args.document)
    else:
        t.process_path(args.harvest)
