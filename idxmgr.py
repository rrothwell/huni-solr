#!/usr/bin/env python

from lxml import etree
import sys
import os.path
import argparse
import logging
import ConfigParser
import requests
from datetime import date
from datetime import datetime

class Index:
    """All the required to manage submission to Solr"""
    def __init__(self, update_url, site):
        self.update_url = update_url
        self.site = site
        self.headers = { 'Content-type': 'text/xml; charset=utf-8' }

    def commit(self):
        """Commit the pending updates"""
        msg = '<commit expungeDeletes="true"/>'
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully committed the changes.")
        else:
            log.error("Something went wrong trying to commit the changes.")
            log.error("\n%s" % resp.text)

    def clean(self):
        """Delete all documents

        If a site is specified, then only the documents of that site will be purged.
        """
        if self.site is not None:
            msg = "<delete><query>prov_site_short:%s</query></delete>" % self.site
        else:
            msg = "<delete><query>*:*</query></delete>"
        resp = requests.post(self.update_url, msg, headers=self.headers)

        log.debug("Purge message: %s" % msg)
        if resp.status_code == 200:
            log.debug("Successfully submitted the index delete request.")
        else:
            log.error("Something went wrong trying to submit a request to wipe the index.")
            log.error("\n%s" % resp.text)

    def optimize(self):
        """Optimize the on disk index"""
        msg = '<optimize waitSearcher="false"/>'
        resp = requests.post(self.update_url, msg, headers=self.headers)
        if resp.status_code == 200:
            log.debug("Successfully optimized the index.")
        else:
            log.error("Something went wrong trying to optimize the index.")
            log.error("\n%s" % resp.text)

if __name__ == "__main__":

    # read and check the options
    parser = argparse.ArgumentParser(description='Solr Index manager: clean and optimize::: DANGER!')
    parser.add_argument('--url', required=True, dest='url', 
        help='The URL of the Solr server; it will be something like http://{host}:{port}/solr/{core}/update?')

    parser.add_argument('--site', dest='site', default=None,
        help="Sometimes you just want to remove a single site - use this option. Pass in the value in \
        prov_site_short and all documents with that field will be purged.")

    parser.add_argument('--clean', dest='clean', action='store_true', help="Wipe the index or a specific site. This is \
        a DESTRUCTIVE OPERATION - you\'ve been warned")

    parser.add_argument('--optimize', dest='optimize', action='store_true', help="Optimize the index. As this is the trigger \
        for updating a slave from a master, it is up to you to call it when you\'re confident the master is ready.")

    parser.add_argument('--debug', dest='debug', action='store_true', help='Turn on full debugging (includes --info)')
    parser.add_argument('--info', dest='info', action='store_true', help='Turn on informational messages')
    args = parser.parse_args()

   # unless we specify otherwise
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    if args.info:
        logging.basicConfig(level=logging.INFO)

    if not (args.debug and args.info):
        # just give us error messages
        logging.basicConfig(level=logging.ERROR)

    # quieten the requests library
    requests_log = logging.getLogger("requests")
    requests_log.setLevel(logging.ERROR)

    # get the logger
    log = logging.getLogger('CLEANER')

    # wipe the index if requested
    index = Index(args.url, args.site)

    # has the user specified a wipe operation
    if args.clean:
        index.clean()
        index.commit()

    # or they do just want to trigger an optimize - which then
    #  triggers an update of the slave
    if args.optimize:
        index.optimize()

